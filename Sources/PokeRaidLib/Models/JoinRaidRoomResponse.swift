import Foundation

public struct JoinRaidRoomResponse: Decodable {
    let message: String?
    let didSucceed: Bool

    var isAuthorized: Bool {
        message != "Auth required"
    }

    enum CodingKeys: String, CodingKey {
        case message = "msg"
        case didSucceed = "success"
    }
}
