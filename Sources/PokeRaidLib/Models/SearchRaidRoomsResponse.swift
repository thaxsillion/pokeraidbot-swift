import Foundation

struct SearchRaidRoomsResponse: Decodable {
    let raidRooms: [RaidRoom]

    enum CodingKeys: String, CodingKey {
        case raidRooms = "raid_rooms"
    }
}
