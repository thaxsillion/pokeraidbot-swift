@preconcurrency import Foundation

public enum HostState: String, Decodable, Sendable {
    case raidClosed = "raid_closed"
    case waitingRequests = "waiting_requests"
    case raidCompleted = "raid_completed"
    case raidStarting = "raid_starting"
}

public struct RaidRoom: Decodable, CustomStringConvertible, Sendable {
    let roomName: String
    let hostRating: Float?
    let hostState: HostState
    let gymName: String
    let remainingInvitesCount: Int
    let creationDate: Date
    let tier: Int

    let activeUntilDate: Date
    let isClosed: Bool
    let isJoined: Bool
    let isVerified: Bool
    let reservationJoinCount: Int
    let roomId: Int
    let roomMemberCount: Int
    let roomOwner: String?
    let userLimit: Int
    let variant: Int?
    let isWeatherBoosted: Bool
    let imageUrl: String
    let gymId: Int
    let pokedexNumber: Int
    let autoJoinedCount: Int

    enum CodingKeys: String, CodingKey {
        case roomName = "room_name"
        case hostRating = "host_rating"
        case hostState = "host_state"
        case gymName = "gym_name"
        case remainingInvitesCount = "remaining_invite"
        case creationDate = "create_date"
        case tier
        case activeUntilDate = "active_until"
        case isClosed = "is_closed"
        case isJoined = "is_joined"
        case isVerified = "is_verified"
        case reservationJoinCount = "reservation_join_count"
        case roomId = "room_id"
        case roomMemberCount = "room_member_count"
        case roomOwner = "room_owner"
        case userLimit = "user_limit"
        case variant
        case isWeatherBoosted = "weather_boost"
        case imageUrl = "image_url"
        case gymId = "gym_id"
        case pokedexNumber = "dex"
        case autoJoinedCount = "auto_joined"
    }

    var isTrusted: Bool {
        (hostRating ?? 0) >= 3.75
    }

    var canBeJoined: Bool {
        let isWaitingForRequests = (hostState == .waitingRequests)
        return isWaitingForRequests && !isClosed && !isFull
    }

    public var description: String {
        "RaidRoom[pokemon=\(roomName), rating=\(hostRating ?? 0), isWeatherBoosted=\(isWeatherBoosted)]"
    }

    public init(
        roomName: String,
        hostRating: Float?,
        hostState: HostState,
        gymName: String,
        remainingInvitesCount: Int,
        creationDate: Date,
        tier: Int,
        activeUntilDate: Date,
        isClosed: Bool,
        isJoined: Bool,
        isVerified: Bool,
        reservationJoinCount: Int,
        roomId: Int,
        roomMemberCount: Int,
        roomOwner: String?,
        userLimit: Int,
        variant: Int?,
        isWeatherBoosted: Bool,
        imageUrl: String,
        gymId: Int,
        pokedexNumber: Int,
        autoJoinedCount: Int
    ) {
        self.roomName = roomName
        self.hostRating = hostRating
        self.hostState = hostState
        self.gymName = gymName
        self.remainingInvitesCount = remainingInvitesCount
        self.creationDate = creationDate
        self.tier = tier
        self.activeUntilDate = activeUntilDate
        self.isClosed = isClosed
        self.isJoined = isJoined
        self.isVerified = isVerified
        self.reservationJoinCount = reservationJoinCount
        self.roomId = roomId
        self.roomMemberCount = roomMemberCount
        self.roomOwner = roomOwner
        self.userLimit = userLimit
        self.variant = variant
        self.isWeatherBoosted = isWeatherBoosted
        self.imageUrl = imageUrl
        self.gymId = gymId
        self.pokedexNumber = pokedexNumber
        self.autoJoinedCount = autoJoinedCount
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        roomName = try container.decode(String.self, forKey: .roomName)
        hostRating = try? container.decode(Float.self, forKey: .hostRating)
        hostState = try container.decode(HostState.self, forKey: .hostState)
        gymName = try container.decode(String.self, forKey: .gymName)
        remainingInvitesCount = try container.decode(Int.self, forKey: .remainingInvitesCount)

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"

        let creationDateString = try container.decode(String.self, forKey: .creationDate)
        creationDate = dateFormatter.date(from: String(creationDateString.prefix(creationDateString.count - 4)))!
        let activeUntilString = try container.decode(String.self, forKey: .activeUntilDate)
        activeUntilDate = dateFormatter.date(from: String(activeUntilString.prefix(activeUntilString.count - 4)))!

        tier = try container.decode(Int.self, forKey: .tier)

        isClosed = try container.decode(Bool.self, forKey: .isClosed)
        isJoined = try container.decode(Bool.self, forKey: .isJoined)
        isVerified = try container.decode(Bool.self, forKey: .isVerified)
        reservationJoinCount = try container.decode(Int.self, forKey: .reservationJoinCount)
        roomId = try container.decode(Int.self, forKey: .roomId)
        roomMemberCount = try container.decode(Int.self, forKey: .roomMemberCount)
        roomOwner = try? container.decode(String.self, forKey: .roomOwner)
        userLimit = try container.decode(Int.self, forKey: .userLimit)
        variant = try? container.decode(Int.self, forKey: .variant)
        isWeatherBoosted = (try container.decode(Bool?.self, forKey: .isWeatherBoosted)) ?? false
        imageUrl = try container.decode(String.self, forKey: .imageUrl)
        gymId = try container.decode(Int.self, forKey: .gymId)
        pokedexNumber = try container.decode(Int.self, forKey: .pokedexNumber)
        autoJoinedCount = try container.decode(Int.self, forKey: .autoJoinedCount)
    }

    var isFull: Bool { remainingInvitesCount <= 0 }
}
