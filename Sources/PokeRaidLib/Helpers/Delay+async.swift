import Foundation

func delay<T: Sendable>(_ milliseconds: Int, closure: @escaping @Sendable () async throws -> T) async throws -> T {
    guard milliseconds > 0 else {
        return try await closure()
    }

    return await withUnsafeContinuation { continuation in
        DispatchQueue.global()
            .asyncAfter(deadline: .now() + .milliseconds(milliseconds)) {
                Task.detached {
                    continuation.resume(returning: try await closure())
                }
            }
    }
}
