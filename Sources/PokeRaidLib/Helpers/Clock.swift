import Foundation

public protocol Clock: Sendable {
    func now() -> Date
}

public final class RealClock: Clock {
    public init() {}

    public func now() -> Date {
        Date()
    }
}
