import Foundation

public final class BotLogHandler {
    public static var didLog: ((String) -> Void)?

    private init() {}
}

private let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    return dateFormatter
}()

public func log(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    let timestamp = dateFormatter.string(from: Date())
    let output = items.map { "\($0)" }.joined(separator: separator)
    let logString = "[\(timestamp)] \(output)"

    print(logString, separator: separator, terminator: terminator)
    BotLogHandler.didLog?(logString)
}
