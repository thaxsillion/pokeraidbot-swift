import Foundation

protocol DelayGenerator {
    func makeDelayInMilliseconds() -> Int
}

final class RandomDelayGenerator: DelayGenerator {
    private let upperBoundMilliseconds = 2000
    private let lowerBoundMilliseconds = 500

    func makeDelayInMilliseconds() -> Int {
        Int.random(in: lowerBoundMilliseconds ... upperBoundMilliseconds)
    }
}
