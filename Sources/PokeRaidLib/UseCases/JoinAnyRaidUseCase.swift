import Foundation

enum JoinAnyRaidError: Error {
    case couldNotJoinAnyRaidRoom
}

public protocol JoinAnyRaidUseCase: Sendable {
    func joinAnyRaid(
        raidRooms: [RaidRoom]
    ) async throws -> RaidRoom
}
