import Foundation

public struct NoMatchingRaidRoomsError: Error {}

public protocol FindRaidRoomsUseCase: Sendable {
    func findRaidRooms(
        pokemonNumber: PokemonNumber?,
        skipRaidRoomIds: [Int]
    ) async throws -> [RaidRoom]
}
