import Foundation

public protocol JoinRaidUseCase {
    func joinRaid(
        pokemonNumber: PokemonNumber?
    ) async throws -> RaidRoom
}
