import NIOHTTP1

struct JoinRaidRoomRequest: Request {
    let body: String? = "{}"
    let path: String
    let headers: [(name: String, value: String)] = []
    let method: HTTPMethod = .POST
    let queryItems: [(name: String, value: String)] = []

    init(raidRoom: RaidRoom) {
        path = "/api/raid_room/join/\(raidRoom.roomId)"
    }
}
