import AsyncHTTPClient
import Foundation
import NIO
import NIOHTTP1

enum PokeRaidClientError: Error {
    case requestConstructionFailure
    case decodingFailure
}

public extension EventLoopFuture {
    /// Get the value/error from an `EventLoopFuture` in an `async` context.
    ///
    /// This function can be used to bridge an `EventLoopFuture` into the `async` world. Ie. if you're in an `async`
    /// function and want to get the result of this future.
    func get() async throws -> Value {
        try await withUnsafeThrowingContinuation { cont in
            self.whenComplete { result in
                switch result {
                case .success(let value):
                    cont.resume(returning: value)
                case .failure(let error):
                    cont.resume(throwing: error)
                }
            }
        }
    }
}

public final class PokeRaidClient: @unchecked Sendable {
    private enum Constants {
        static let version = "43"
        static let baseUrl = "https://poketrade.me"
    }

    private enum QueryItemName {
        static let version = "version"
        static let appName = "app_name"
        static let country = "country"
        static let platform = "platform"
        static let language = "language"
        static let registrationToken = "registration_token"
    }

    private let httpClient: HTTPClient
    private let cookie: String
    private let registrationToken: String

    private lazy var baseHeaders: [(String, String)] = {
        [
            ("Accept", "*/*"),
            ("User-Agent", "PokeRaid/\(Constants.version) CFNetwork/1303.1 Darwin/21.0.0"),
            ("Accept-Language", "en-us"),
            ("Cookie", cookie)
        ]
    }()

    private let baseQueryItems: [URLQueryItem] = {
        [
            URLQueryItem(name: QueryItemName.version, value: Constants.version),
            URLQueryItem(name: QueryItemName.appName, value: "pokeraid"),
            URLQueryItem(name: QueryItemName.country, value: "PL"),
            URLQueryItem(name: QueryItemName.platform, value: "ios"),
            URLQueryItem(name: QueryItemName.language, value: "en")
        ]
    }()

    deinit {
        try? httpClient.syncShutdown()
    }

    public init(session: String, cfduid: String, registrationToken: String) {
        httpClient = HTTPClient(eventLoopGroupProvider: .createNew)

        cookie = "session=\(session); __cfduid=\(cfduid)"
        self.registrationToken = registrationToken
    }

    func fetch<Model: Decodable>(request: Request) async throws -> Model {
        do {
            var urlComponents = try makeBaseUrlComponents(withPath: request.path)

            var queryItems = request.queryItems.map(URLQueryItem.init)
            queryItems.append(URLQueryItem(name: QueryItemName.registrationToken, value: registrationToken))

            urlComponents.queryItems?.append(contentsOf: queryItems)

            guard let url = urlComponents.url else { throw PokeRaidClientError.requestConstructionFailure }

            let headers = HTTPHeaders(baseHeaders + request.headers)
            let body = request.body.map(HTTPClient.Body.string)

            let request = try HTTPClient.Request(
                url: url,
                method: request.method,
                headers: headers,
                body: body
            )

            return try await httpClient.execute(request: request)
                .flatMapThrowing { response -> Model in
                    guard var body = response.body,
                          let data = body.readData(length: body.readableBytes)
                    else {
                        throw PokeRaidClientError.decodingFailure
                    }

                    do {
                        return try JSONDecoder().decode(Model.self, from: data)
                    } catch {
                        print(String(data: data, encoding: .utf8)!)
                        print(error)
                        throw PokeRaidClientError.decodingFailure
                    }
                }.get()
        } catch {
            throw error
        }
    }

    private func makeBaseUrlComponents(withPath path: String) throws -> URLComponents {
        guard var urlComponents = URLComponents(string: Constants.baseUrl + path) else {
            throw PokeRaidClientError.requestConstructionFailure
        }

        urlComponents.queryItems = baseQueryItems
        return urlComponents
    }
}
