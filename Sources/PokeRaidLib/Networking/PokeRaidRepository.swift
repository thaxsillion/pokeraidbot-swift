import Foundation
import NIO

public protocol RaidRoomRepository: Sendable {
    func fetchRaidRooms(pokemonNumber: PokemonNumber?) async throws -> [RaidRoom]
    func joinRaidRoom(raidRoom: RaidRoom) async throws -> JoinRaidRoomResponse
}

public final class PokeRaidRepository: RaidRoomRepository {
    private let client: PokeRaidClient

    public init(client: PokeRaidClient) {
        self.client = client
    }

    public func fetchRaidRooms(pokemonNumber: PokemonNumber?) async throws -> [RaidRoom] {
        let response: SearchRaidRoomsResponse = try await client.fetch(
            request: FetchRaidRoomsRequest(pokedexNumber: pokemonNumber)
        )
        return response.raidRooms
    }

    public func joinRaidRoom(raidRoom: RaidRoom) async throws -> JoinRaidRoomResponse {
        try await client.fetch(request: JoinRaidRoomRequest(raidRoom: raidRoom))
    }
}
