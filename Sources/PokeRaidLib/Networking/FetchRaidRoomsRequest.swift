import NIOHTTP1

struct FetchRaidRoomsRequest: Request {
    let body: String? = nil
    let path: String = "/api/raid_room/search"
    let headers: [(name: String, value: String)] = []
    let method: HTTPMethod = .GET
    var queryItems: [(name: String, value: String)] = [
        ("tier", ""),
        ("only_active", "true"),
        ("only_empty", "false")
    ]

    init(pokedexNumber: PokemonNumber?) {
        if let pokedexNumber = pokedexNumber {
            queryItems.append(("dex", String(pokedexNumber)))
        }
    }
}
