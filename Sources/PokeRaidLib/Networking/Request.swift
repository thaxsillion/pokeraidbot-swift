import NIOHTTP1

protocol Request {
    var path: String { get }
    var queryItems: [(name: String, value: String)] { get }
    var headers: [(name: String, value: String)] { get }
    var method: HTTPMethod { get }
    var body: String? { get }
}
