import Foundation

public actor JoinRaidService: JoinRaidUseCase {
    private let joinAnyRaidUseCase: JoinAnyRaidUseCase
    private let findRaidRoomsUseCase: FindRaidRoomsUseCase

    var joinRaidRoomRetryPolicy: RetryPolicy = AlwaysRetryPolicy()
    var delayGenerator: DelayGenerator = RandomDelayGenerator()

    public init(
        joinAnyRaidUseCase: JoinAnyRaidUseCase,
        findRaidRoomsUseCase: FindRaidRoomsUseCase
    ) {
        self.joinAnyRaidUseCase = joinAnyRaidUseCase
        self.findRaidRoomsUseCase = findRaidRoomsUseCase
    }

    public func joinRaid(pokemonNumber: Int?) async throws -> RaidRoom {
        try await joinRaid(pokemonNumber: pokemonNumber, skipRaidRoomIds: [])
    }

    private func joinRaid(
        pokemonNumber: PokemonNumber?,
        skipRaidRoomIds: [Int] = []
    ) async throws -> RaidRoom {
        var skipRaidRoomIds = skipRaidRoomIds

        let matchingRaidRooms = try await findRaidRoomsUseCase.findRaidRooms(
            pokemonNumber: pokemonNumber,
            skipRaidRoomIds: skipRaidRoomIds
        ).sorted { $0.isWeatherBoosted && $1.isWeatherBoosted }

        do {
            return try await joinAnyRaidUseCase.joinAnyRaid(raidRooms: matchingRaidRooms)
        } catch {
            joinRaidRoomRetryPolicy.increaseFailedAttempts()
            skipRaidRoomIds.append(contentsOf: matchingRaidRooms.map(\.roomId))

            if error is AuthError {
                throw error
            } else if let joinAnyRaidError = error as? JoinAnyRaidError {
                return try await handleJoinAnyRaidError(
                    joinAnyRaidError,
                    pokemonNumber: pokemonNumber,
                    skipRaidRoomIds: skipRaidRoomIds
                )
            } else if error is NoMatchingRaidRoomsError {
                throw error
            } else {
                throw error
            }
        }
    }

    private func handleJoinAnyRaidError(
        _ error: JoinAnyRaidError,
        pokemonNumber: PokemonNumber?,
        skipRaidRoomIds: [Int]
    ) async throws -> RaidRoom {
        switch error {
        case .couldNotJoinAnyRaidRoom:
            if joinRaidRoomRetryPolicy.canRetry() {
                return try await delay(delayGenerator.makeDelayInMilliseconds()) {
                    try await self.joinRaid(pokemonNumber: pokemonNumber, skipRaidRoomIds: skipRaidRoomIds)
                }
            } else {
                throw error
            }
        }
    }
}
