import Foundation

public actor JoinAnyRaidService: JoinAnyRaidUseCase {
    private let raidRoomRepository: RaidRoomRepository

    public init(raidRoomRepository: RaidRoomRepository) {
        self.raidRoomRepository = raidRoomRepository
    }

    public func joinAnyRaid(
        raidRooms: [RaidRoom]
    ) async throws -> RaidRoom {
        try await _joinAnyRaid(raidRooms: raidRooms, index: 0)
    }

    private func _joinAnyRaid(
        raidRooms: [RaidRoom],
        index: Int = 0
    ) async throws -> RaidRoom {
        guard index <= raidRooms.count - 1 else {
            log("Failing because couldn't join any raid room")
            throw JoinAnyRaidError.couldNotJoinAnyRaidRoom
        }

        let selectedRaidRoom = raidRooms[index]

        log("Trying to join raid room \(index + 1)/\(raidRooms.count)...")

        do {
            let response = try await joinRaidRoom(selectedRaidRoom)

            guard response.didSucceed else {
                switch response.isAuthorized {
                case true: throw JoinAnyRaidError.couldNotJoinAnyRaidRoom
                case false: throw AuthError.unauthorized
                }
            }

            return selectedRaidRoom
        } catch {
            if let authError = error as? AuthError, case AuthError.unauthorized = authError {
                throw error
            }

            return try await _joinAnyRaid(raidRooms: raidRooms, index: index + 1)
        }
    }

    private func joinRaidRoom(_ raidRoom: RaidRoom) async throws -> JoinRaidRoomResponse {
        log("Joining...")
        return try await raidRoomRepository.joinRaidRoom(raidRoom: raidRoom)
    }
}
