import Foundation

public actor FindMatchingRaidRoomsService: FindRaidRoomsUseCase {
    private let raidRoomRepository: RaidRoomRepository
    private let raidRoomFilterPolicy: RaidRoomFilterPolicy

    var delayGenerator: DelayGenerator = RandomDelayGenerator()
    var findRaidRoomsRetryPolicy: RetryPolicy = AlwaysRetryPolicy()

    public init(
        raidRoomRepository: RaidRoomRepository,
        raidRoomFilterPolicy: RaidRoomFilterPolicy
    ) {
        self.raidRoomRepository = raidRoomRepository
        self.raidRoomFilterPolicy = raidRoomFilterPolicy
    }

    public func findRaidRooms(
        pokemonNumber: PokemonNumber?,
        skipRaidRoomIds: [Int]
    ) async throws -> [RaidRoom] {
        try await findMatchingRaidRooms(pokemonNumber: pokemonNumber, skipRaidRoomIds: skipRaidRoomIds, attempt: 1)
    }

    private func findMatchingRaidRooms(
        pokemonNumber: PokemonNumber?,
        skipRaidRoomIds: [Int],
        attempt: Int
    ) async throws -> [RaidRoom] {
        log("[\(attempt)] Searching matching raid rooms.")

        let unfilteredRaidRooms = try await fetchRaidRooms(pokemonNumber: pokemonNumber)

        if !unfilteredRaidRooms.isEmpty {
            log("Unfiltered count: \(unfilteredRaidRooms.count)")
        }

        let raidRooms = unfilteredRaidRooms
            .filter { !skipRaidRoomIds.contains($0.roomId) }
            .filter(raidRoomFilterPolicy.filter)

        if raidRooms.isEmpty {
            findRaidRoomsRetryPolicy.increaseFailedAttempts()

            if findRaidRoomsRetryPolicy.canRetry() {
                return try await delay(delayGenerator.makeDelayInMilliseconds()) {
                    try await self.findMatchingRaidRooms(
                        pokemonNumber: pokemonNumber,
                        skipRaidRoomIds: skipRaidRoomIds,
                        attempt: attempt + 1
                    )
                }
            } else {
                throw NoMatchingRaidRoomsError()
            }
        }

        return raidRooms
    }

    private func fetchRaidRooms(pokemonNumber: Int?) async throws -> [RaidRoom] {
        try await raidRoomRepository.fetchRaidRooms(pokemonNumber: pokemonNumber)
    }
}
