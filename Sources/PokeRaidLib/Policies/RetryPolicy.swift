import Foundation

protocol RetryPolicy {
    func increaseFailedAttempts()
    func canRetry() -> Bool
}

final class AlwaysRetryPolicy: RetryPolicy {
    func increaseFailedAttempts() {}

    func canRetry() -> Bool {
        true
    }
}
