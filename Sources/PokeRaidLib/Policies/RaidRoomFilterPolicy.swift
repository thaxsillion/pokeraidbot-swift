import Foundation

public protocol RaidRoomFilterPolicy: Sendable {
    func filter(_ room: RaidRoom) -> Bool
}

public final class BeneficialRaidRoomFilterPolicy: RaidRoomFilterPolicy, @unchecked Sendable {
    private let calendar: Calendar
    private let clock: Clock

    public init(calendar: Calendar, clock: Clock) {
        self.calendar = calendar
        self.clock = clock
    }

    public func filter(_ room: RaidRoom) -> Bool {
        guard let raidRoomAgeInMinutes = calendar.minutes(
            between: clock.now(),
            and: room.creationDate
        ) else {
            return false
        }

        let isRoomFresh = (raidRoomAgeInMinutes <= 2)

        return isRoomFresh &&
            room.isTrusted &&
            room.canBeJoined //&&
            //room.isWeatherBoosted
    }
}

private extension Calendar {
    func minutes(between date1: Date, and date2: Date) -> Int? {
        let date1Components = dateComponents([.hour, .minute], from: date1)
        let date2Components = dateComponents([.hour, .minute], from: date2)
        return dateComponents([.minute], from: date2Components, to: date1Components).minute
    }
}
