import _Concurrency
import Dispatch
import Foundation

public final class Bot {
    private let joinRaidUseCase: JoinRaidUseCase

    public init(joinRaidUseCase: JoinRaidUseCase) {
        self.joinRaidUseCase = joinRaidUseCase
    }

    public func joinRaid(
        pokemonNumber: PokemonNumber?
    ) async throws -> RaidRoom {
        try await joinRaidUseCase.joinRaid(pokemonNumber: pokemonNumber)
    }
}
