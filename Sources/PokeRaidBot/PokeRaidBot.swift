import Foundation
import NIO
import PokeRaidLib

@main
enum PokeRaidBot {
    static func main() async throws {
        let env = ProcessInfo.processInfo.environment
        let args = ProcessInfo.processInfo.arguments

        guard let configuration = BotConfiguration(environment: env, arguments: args) else {
            print("Invalid Bot configuration.")
            exit(EXIT_FAILURE)
        }

        let bot = BotFactory().create(configuration: configuration)

        do {
            let raidRoom = try await bot.joinRaid(pokemonNumber: configuration.pokemonNumber)

            log("Successfully joined \(raidRoom)")
        } catch {
            log("Bot error: \(error)")
        }
    }
}
