import PokeRaidLib
import Foundation

final class BotFactory {
    func create(configuration: BotConfiguration) -> Bot {
        let raidRoomRepository = PokeRaidRepository(
            client: PokeRaidClient(
                session: configuration.session,
                cfduid: configuration.cfduid,
                registrationToken: configuration.registrationToken
            )
        )

        let findRaidRoomsService = makeFindMatchingRaidRoomsService(raidRoomRepository: raidRoomRepository)
        let bot = Bot(
            joinRaidUseCase: JoinRaidService(
                joinAnyRaidUseCase: JoinAnyRaidService(raidRoomRepository: raidRoomRepository),
                findRaidRoomsUseCase: findRaidRoomsService
            )
        )

        return bot
    }

    private func makeFindMatchingRaidRoomsService(
        raidRoomRepository: PokeRaidRepository
    ) -> FindMatchingRaidRoomsService {
        return FindMatchingRaidRoomsService(
            raidRoomRepository: raidRoomRepository,
            raidRoomFilterPolicy: BeneficialRaidRoomFilterPolicy(
                calendar: Calendar.current,
                clock: RealClock()
            )
        )
    }
}
