import Foundation
import PokeRaidLib

struct BotConfiguration {
    let session: String
    let cfduid: String
    let registrationToken: String
    let pokemonNumber: PokemonNumber?

    init?(environment: [String: String], arguments: [String]) {
        guard let session = environment["session"],
              let cfduid = environment["cfduid"],
              let registrationToken = environment["registrationToken"],
              arguments.count > 0
        else {
            return nil
        }

        self.session = session
        self.cfduid = cfduid
        self.registrationToken = registrationToken
        pokemonNumber = Int(arguments[1])
    }
}
