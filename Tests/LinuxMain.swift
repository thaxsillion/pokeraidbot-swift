import XCTest

import PokeRaidBotTests

var tests = [XCTestCaseEntry]()
tests += PokeRaidBotTests.allTests()
XCTMain(tests)
