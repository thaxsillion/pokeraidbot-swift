//@testable import PokeRaidLib
//import XCTest
//
//final class FindMatchingRaidRoomsServiceTests: XCTestCase {
//    func testShouldRetrySearchingMatchingRoomsUntilFound() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            var currentRetryCount = 0
//
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                if currentRetryCount == 3 {
//                    return [raidRoom()]
//                }
//                currentRetryCount += 1
//                return [raidRoom(isWeatherBoosted: false)]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: nil, success: true)
//            })
//
//            let sut = FindMatchingRaidRoomsService(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//
//            // When
//            _ = try await sut.findMatchingRaidRooms(pokemonNumber: nil, skipRaidRoomIds: [])
//
//            // Then
//            XCTAssertEqual(currentRetryCount, 3)
//            expectation.fulfill()
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    func testShouldMatchValidRaidRoom() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                [raidRoom()]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: nil, success: true)
//            })
//
//            let sut = FindMatchingRaidRoomsService(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//
//            // When
//            _ = try await sut.findMatchingRaidRooms(pokemonNumber: nil, skipRaidRoomIds: [])
//
//            // Then
//            do {
//                _ = try await sut.findMatchingRaidRooms(pokemonNumber: nil, skipRaidRoomIds: [])
//            } catch {
//                XCTFail()
//            }
//
//            expectation.fulfill()
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    func testShouldNotMatchRaidRoomWithBadHostReputation() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                [raidRoom(hostRating: 2.5)]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: nil, success: true)
//            })
//
//            let sut = FindMatchingRaidRoomsService(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//            sut.findRaidRoomsRetryPolicy = NeverRetryPolicy()
//
//            // Then
//            do {
//                _ = try await sut.findMatchingRaidRooms(pokemonNumber: nil, skipRaidRoomIds: [])
//                XCTFail()
//            } catch {}
//
//            expectation.fulfill()
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    func testShouldNotMatchRaidRoomWithNoRemainingInvitesLeft() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                [raidRoom(remainingInvitesCount: 0)]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: nil, success: true)
//            })
//
//            let sut = FindMatchingRaidRoomsService(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//            sut.findRaidRoomsRetryPolicy = NeverRetryPolicy()
//
//            // Then
//            do {
//                _ = try await sut.findMatchingRaidRooms(pokemonNumber: nil, skipRaidRoomIds: [])
//                XCTFail()
//            } catch {}
//            expectation.fulfill()
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    func testShouldNotMatchRaidRoomWhichIsClosed() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                [raidRoom(isClosed: true)]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: nil, success: true)
//            })
//
//            let sut = FindMatchingRaidRoomsService(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//            sut.findRaidRoomsRetryPolicy = NeverRetryPolicy()
//
//            // Then
//            do {
//                _ = try await sut.findMatchingRaidRooms(pokemonNumber: nil, skipRaidRoomIds: [])
//                XCTFail()
//            } catch {}
//            expectation.fulfill()
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    static var allTests = [
//        ("testShouldRetrySearchingMatchingRoomsUntilFound", testShouldRetrySearchingMatchingRoomsUntilFound),
//        ("testShouldNotMatchRaidRoomWithBadHostReputation", testShouldNotMatchRaidRoomWithBadHostReputation),
//        ("testShouldNotMatchRaidRoomWithNoRemainingInvitesLeft", testShouldNotMatchRaidRoomWithNoRemainingInvitesLeft),
//        ("testShouldNotMatchRaidRoomWhichIsClosed", testShouldNotMatchRaidRoomWhichIsClosed),
//    ]
//}
