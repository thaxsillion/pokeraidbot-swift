//@testable import PokeRaidLib
//import XCTest
//
//final class JoinRaidServiceTests: XCTestCase {
//    func testShouldFailWithUnauthorizedErrorIfUnauthorizedSentInJoinRaidRoomResponse() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                [raidRoom()]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: "Auth required", success: false)
//            })
//
//            let sut = makeBot(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//
//            do {
//                _ = try await sut.joinRaid(pokemonNumber: nil)
//                XCTFail("Bot.joinRaid should throw when unauthorized.")
//            } catch {
//                expectation.fulfill()
//            }
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    func testShouldFavorWeatherBoostedRaidRooms() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                [
//                    raidRoom(name: "Room - no WB", isWeatherBoosted: false),
//                    raidRoom(name: "Room - no WB", isWeatherBoosted: false),
//                    raidRoom(name: "Room - WB", isWeatherBoosted: true),
//                    raidRoom(name: "Room - no WB", isWeatherBoosted: false),
//                ]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: nil, success: true)
//            })
//
//            let sut = makeBot(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//
//            // When
//            let room = try await sut.joinRaid(pokemonNumber: nil)
//
//            // Then
//            XCTAssertEqual(room.roomName, "Room - WB")
//
//            expectation.fulfill()
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    func testShouldRetrySearchingMatchingRoomsUntilFound() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            var currentRetryCount = 0
//
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                if currentRetryCount == 3 {
//                    return [raidRoom()]
//                }
//                currentRetryCount += 1
//                return [raidRoom(isWeatherBoosted: false)]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: nil, success: true)
//            })
//
//            let sut = makeBot(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//
//            // When
//            _ = try await sut.joinRaid(pokemonNumber: nil)
//
//            // Then
//            XCTAssertEqual(currentRetryCount, 3)
//            expectation.fulfill()
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    func testShouldMatchValidRaidRoom() throws {
//        let expectation = expectation(description: "Should finish")
//
//        Task.detached {
//            // Given
//            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
//                [raidRoom()]
//            }, joinRaidRoomMock: { _ in
//                JoinRaidRoomResponse(message: nil, success: true)
//            })
//
//            let sut = makeBot(raidRoomRepository: raidRoomRepository)
//            sut.delayGenerator = StaticDelayGenerator(delay: 0)
//
//            // Then
//            do {
//                _ = try await sut.joinRaid(pokemonNumber: nil)
//            } catch {
//                XCTFail()
//            }
//
//            expectation.fulfill()
//        }
//
//        wait(for: [expectation], timeout: 1.0)
//    }
//
//    static var allTests = [
//        ("testShouldFavorWeatherBoostedRaidRooms", testShouldFavorWeatherBoostedRaidRooms),
//        ("testShouldFailWithUnauthorizedErrorIfUnauthorizedSentInJoinRaidRoomResponse", testShouldFailWithUnauthorizedErrorIfUnauthorizedSentInJoinRaidRoomResponse),
//        ("testShouldRetrySearchingMatchingRoomsUntilFound", testShouldRetrySearchingMatchingRoomsUntilFound),
//    ]
//}
//
//private func makeBot(raidRoomRepository: RaidRoomRepository) -> JoinRaidService {
//    let joinAnyRaidUseCase = JoinAnyRaidService(raidRoomRepository: raidRoomRepository)
//    let findMatchingRaidRoomsUseCase = FindMatchingRaidRoomsService(raidRoomRepository: raidRoomRepository)
//    findMatchingRaidRoomsUseCase.delayGenerator = StaticDelayGenerator(delay: 0)
//
//    return JoinRaidService(
//        joinAnyRaidUseCase: joinAnyRaidUseCase,
//        findMatchingRaidRoomsUseCase: findMatchingRaidRoomsUseCase
//    )
//}
//
//private enum AnyError: Error {
//    case any
//}
