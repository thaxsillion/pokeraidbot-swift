@testable import PokeRaidLib
import XCTest

final class JoinAnyRaidServiceTests: XCTestCase {
    func testShouldFailWithUnauthorizedErrorIfUnauthorizedSentInJoinRaidRoomResponse() throws {
        let expectation = expectation(description: "Should finish")

        Task.detached {
            // Given
            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
                [raidRoom()]
            }, joinRaidRoomMock: { _ in
                JoinRaidRoomResponse(message: "Auth required", didSucceed: false)
            })

            let sut = JoinAnyRaidService(raidRoomRepository: raidRoomRepository)

            do {
                _ = try await sut.joinAnyRaid(raidRooms: [raidRoom()])
                XCTFail("Bot.joinRaid should throw when unauthorized.")
            } catch {
                guard let authError = error as? AuthError else {
                    XCTFail("Error should be of type AuthError.unauthorized")
                    return
                }

                if case AuthError.unauthorized = authError {} else {
                    XCTFail("Error should be of type AuthError.unauthorized")
                }
                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testShouldThrowCouldNotJoinRaidRoomIfNoneRaidRoomsFit() throws {
        let expectation = expectation(description: "Should finish")

        Task.detached {
            // Given
            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
                [raidRoom()]
            }, joinRaidRoomMock: { _ in
                JoinRaidRoomResponse(message: nil, didSucceed: false)
            })

            let sut = JoinAnyRaidService(raidRoomRepository: raidRoomRepository)

            do {
                _ = try await sut.joinAnyRaid(raidRooms: [raidRoom(), raidRoom(), raidRoom()])
                XCTFail("Bot.joinRaid should throw when no matching raid rooms found.")
            } catch {
                guard let error = error as? JoinAnyRaidError else {
                    XCTFail("Error should be of type JoinAnyRaidError.couldNotJoinAnyRaidRoom")
                    return
                }

                if case JoinAnyRaidError.couldNotJoinAnyRaidRoom = error {} else {
                    XCTFail("Error should be of type JoinAnyRaidError.couldNotJoinAnyRaidRoom")
                }

                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testShouldShouldTryToJoinEachRaidRoomOnce() throws {
        let expectation = expectation(description: "Should finish")

        Task.detached {
            // Given
            var retryAttempts = 0

            let raidRoomRepository = MockRaidRoomRepository(fetchRaidRoomsMock: { _ in
                [raidRoom()]
            }, joinRaidRoomMock: { _ in
                retryAttempts += 1
                return JoinRaidRoomResponse(message: nil, didSucceed: false)
            })

            let sut = JoinAnyRaidService(raidRoomRepository: raidRoomRepository)

            do {
                _ = try await sut.joinAnyRaid(raidRooms: [raidRoom(), raidRoom(), raidRoom()])
                XCTFail("Bot.joinRaid should throw when no matching raid rooms found.")
            } catch {
                XCTAssertEqual(retryAttempts, 3)
                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    static var allTests = [
        ("testShouldFailWithUnauthorizedErrorIfUnauthorizedSentInJoinRaidRoomResponse", testShouldFailWithUnauthorizedErrorIfUnauthorizedSentInJoinRaidRoomResponse),
    ]
}
