@testable import PokeRaidLib

final class MockRaidRoomRepository: RaidRoomRepository {
    let fetchRaidRoomsMock: (PokemonNumber?) async throws -> [RaidRoom]
    let joinRaidRoomMock: (RaidRoom) async throws -> JoinRaidRoomResponse

    init(
        fetchRaidRoomsMock: @escaping (PokemonNumber?) async throws -> [RaidRoom],
        joinRaidRoomMock: @escaping (RaidRoom) async throws -> JoinRaidRoomResponse
    ) {
        self.fetchRaidRoomsMock = fetchRaidRoomsMock
        self.joinRaidRoomMock = joinRaidRoomMock
    }

    func fetchRaidRooms(pokemonNumber: PokemonNumber?) async throws -> [RaidRoom] {
        try await fetchRaidRoomsMock(pokemonNumber)
    }

    func joinRaidRoom(raidRoom: RaidRoom) async throws -> JoinRaidRoomResponse {
        try await joinRaidRoomMock(raidRoom)
    }
}
