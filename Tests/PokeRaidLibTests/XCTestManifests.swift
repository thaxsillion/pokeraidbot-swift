import XCTest

#if !canImport(ObjectiveC)
    public func allTests() -> [XCTestCaseEntry] {
        [
            testCase(PokeRaidBotTests.allTests),
        ]
    }
#endif
