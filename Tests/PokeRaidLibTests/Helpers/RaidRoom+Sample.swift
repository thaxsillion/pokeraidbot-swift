import Foundation
@testable import PokeRaidLib

func raidRoom(
    name: String = "RaidRoom",
    hostRating: Float = 4.5,
    hostState: HostState = .waitingRequests,
    remainingInvitesCount: Int = 1,
    isClosed: Bool = false,
    isWeatherBoosted: Bool = true
) -> RaidRoom {
    RaidRoom(
        roomName: name,
        hostRating: hostRating,
        hostState: hostState,
        gymName: "GymName",
        remainingInvitesCount: remainingInvitesCount,
        creationDate: Date(),
        tier: 3,
        activeUntilDate: Date(),
        isClosed: isClosed,
        isJoined: false,
        isVerified: true,
        reservationJoinCount: 2,
        roomId: 1337,
        roomMemberCount: 5,
        roomOwner: nil,
        userLimit: 10,
        variant: nil,
        isWeatherBoosted: isWeatherBoosted,
        imageUrl: "",
        gymId: 3,
        pokedexNumber: 543,
        autoJoinedCount: 0
    )
}
