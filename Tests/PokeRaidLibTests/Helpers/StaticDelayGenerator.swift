@testable import PokeRaidLib

final class StaticDelayGenerator: DelayGenerator {
    private let delay: Int

    init(delay: Int) {
        self.delay = delay
    }

    func makeDelayInMilliseconds() -> Int {
        delay
    }
}
