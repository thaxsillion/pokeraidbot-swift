@testable import PokeRaidLib

final class NeverRetryPolicy: RetryPolicy {
    func increaseFailedAttempts() {}

    func canRetry() -> Bool {
        false
    }
}
