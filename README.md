# PokeRaidBot

Bot for the PokeRaid app.

Environment variables needed:

- cfduid
- session
- registrationToken

To join a raid with a specific pokemon, pass its number as a direct argument, e.g. `./pokeraid-swift 888`.
