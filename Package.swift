// swift-tools-version:5.5
import PackageDescription

let package = Package(
    name: "PokeRaidBot",
    platforms: [.macOS(.v12)],
    products: [
        .executable(name: "PokeRaidBot", targets: ["PokeRaidBot"]),
        .library(name: "PokeRaidLib", targets: ["PokeRaidLib"]),
    ],
    dependencies: [
        .package(url: "https://github.com/apple/swift-nio", from: "2.13.0"),
        .package(url: "https://github.com/swift-server/async-http-client.git", from: "1.0.0"),
    ],
    targets: [
        .executableTarget(
            name: "PokeRaidBot",
            dependencies: [
                .target(name: "PokeRaidLib"),
            ],
            swiftSettings: []
        ),
        .target(
            name: "PokeRaidLib",
            dependencies: [
                .product(name: "AsyncHTTPClient", package: "async-http-client"),
                .product(name: "_NIOConcurrency", package: "swift-nio"),
            ],
            swiftSettings: [.unsafeFlags(["-warn-concurrency", "-enable-actor-data-race-checks"])]
        ),
        .testTarget(
            name: "PokeRaidLibTests",
            dependencies: [
                .target(name: "PokeRaidBot"),
                .product(name: "AsyncHTTPClient", package: "async-http-client"),
            ],
            swiftSettings: []
        ),
    ]
)
